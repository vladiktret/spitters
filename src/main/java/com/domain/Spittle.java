package com.domain;

import org.springframework.format.annotation.DateTimeFormat;

import javax.persistence.*;
import javax.validation.constraints.NotNull;
import javax.validation.constraints.Size;
import java.io.Serializable;
import java.util.Date;

import static javax.persistence.GenerationType.SEQUENCE;
import static org.apache.commons.lang.builder.EqualsBuilder.reflectionEquals;
import static org.apache.commons.lang.builder.HashCodeBuilder.reflectionHashCode;
import static org.apache.commons.lang.builder.ToStringBuilder.reflectionToString;

@Entity
@Table(name="spittle")
public class Spittle implements Serializable {
  private Long id;
  private Spitter spitter;
  private String text;
  
  @DateTimeFormat(pattern="hh:mma MMM d, YYYY")
  private Date created;
  
  @Id
  @SequenceGenerator(name="spittle_seq", sequenceName="spittle_sequence")
  @GeneratedValue(strategy = SEQUENCE, generator = "spittle_seq")
  public Long getId() {
    return this.id;
  }
  
  public void setId(Long id) {
    this.id = id;
  }
  
  @Column(name="text")
  @NotNull
  @Size(min=1, max=140)
  public String getText() {
    return this.text;
  }
  
  public void setText(String text) {
    this.text = text;
  }

  @Column(name="posted_time")
  public Date getCreated() {
    return this.created;
  }

  public void setCreated(Date created) {
    this.created = created;
  }

  @ManyToOne
  @JoinColumn(name="spitter_id")
  public Spitter getSpitter() {
    return this.spitter;
  }

  public void setSpitter(Spitter spitter) {
    this.spitter = spitter;
  }
  
  // plumbing
  @Override
  public boolean equals(Object obj) {
    return reflectionEquals(this, obj);
  }
  
  @Override
  public int hashCode() {
    return reflectionHashCode(this);
  }
  
  @Override
  public String toString() {
    return reflectionToString(this);
  }
}
