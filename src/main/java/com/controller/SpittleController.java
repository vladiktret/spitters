package com.controller;

import com.domain.Spitter;
import com.domain.Spittle;
import com.service.SpitterService;
import org.springframework.http.HttpStatus;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.security.core.userdetails.User;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.validation.BindException;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.annotation.*;

import javax.inject.Inject;
import javax.servlet.http.HttpServletResponse;
import javax.validation.Valid;

@Controller
@RequestMapping("/spittles")
public class SpittleController {
    private SpitterService spitterService;

    @Inject
    public SpittleController(SpitterService spitterService) {
        this.spitterService = spitterService;
    }

    @RequestMapping(method = RequestMethod.GET)
    public String getRecentSpittles(Model model) {
        model.addAttribute(spitterService.getRecentSpittles(20));
        return "spittles/list";
    }

    @RequestMapping(method = RequestMethod.POST, headers = "Accept=text/html")
    public String createSpittleFromForm(@Valid Spittle spittle,
                                        BindingResult result, HttpServletResponse response)
            throws BindException {
        if (result.hasErrors()) {
            throw new BindException(result);
        }

        spittle.setSpitter(getSpitter());
        spitterService.saveSpittle(spittle);

        return "redirect:/";
    }

    @RequestMapping(method = RequestMethod.POST)
    @ResponseStatus(HttpStatus.CREATED)
    public
    @ResponseBody
    Spittle createSpittle(@Valid Spittle spittle,
                          BindingResult result, HttpServletResponse response)
            throws BindException {
        if (result.hasErrors()) {
            throw new BindException(result);
        }

        spittle.setSpitter(getSpitter());
        spitterService.saveSpittle(spittle);


        response.setHeader("Location", "/spittles/" + spittle.getId());
        return spittle;
    }

    private Spitter getSpitter(){
        User user = (User) SecurityContextHolder.getContext().getAuthentication().getPrincipal();
        return spitterService.getSpitter(user.getUsername());
    }

    @RequestMapping(value = "/{id}", method = RequestMethod.PUT)
    public String updateSpittle(@PathVariable("id") long id,
                                @Valid Spittle spittle) {
        spitterService.saveSpittle(spittle);
        return "spittles/view";
    }

    @RequestMapping(value = "/{id}", method = RequestMethod.GET)
    public String getSpittle(@PathVariable("id") long id,
                             Model model) {
        model.addAttribute(spitterService.getSpittleById(id));
        return "spittles/view";
    }

    @RequestMapping(value = "/{id}", method = RequestMethod.DELETE)
    @ResponseStatus(HttpStatus.NO_CONTENT)
    public void deleteSpittle(@PathVariable("id") long id) {
        spitterService.deleteSpittle(id);
    }
}
