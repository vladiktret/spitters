package com.controller;

import com.domain.Spittle;
import org.springframework.ui.ModelMap;
import org.springframework.web.context.request.WebRequest;
import org.springframework.web.context.request.WebRequestInterceptor;

public class SpittleBackingBeanInterceptor
        implements WebRequestInterceptor {

    public void afterCompletion(WebRequest webRequest,
                                Exception arg1) throws Exception {
    }

    public void postHandle(WebRequest webRequest, ModelMap model)
            throws Exception {
        if (model != null) {
            model.addAttribute(new Spittle());
        }
    }

    public void preHandle(WebRequest webRequest) throws Exception {
    }

}
