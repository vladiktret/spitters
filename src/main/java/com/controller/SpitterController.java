package com.controller;

import com.domain.Spitter;
import com.domain.Spittle;
import com.service.SpitterService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.multipart.MultipartFile;

import javax.inject.Inject;
import javax.servlet.ServletContext;
import javax.validation.Valid;
import java.util.List;
import java.util.Map;

@Controller
@RequestMapping("/spitters")
public class SpitterController {
    private final SpitterService spitterService;

    @Autowired
    private ServletContext servletContext;

    @Inject
    public SpitterController(SpitterService spitterService) {
        this.spitterService = spitterService;
    }

    @RequestMapping(method = RequestMethod.GET)
    public String listSpitters(
            @RequestParam(value = "page", defaultValue = "1") int page,
            @RequestParam(value = "perPage", defaultValue = "10") int perPage,
            Map<String, Object> model) {
        model.put("spitters", spitterService.getAllSpitters());
        return "spitters/list";
    }

    @RequestMapping(method = RequestMethod.GET, params = "new")
    public String createSpitterProfile(Model model) {
        model.addAttribute(new Spitter());
        return "spitters/edit";
    }

    @RequestMapping(method = RequestMethod.POST)
    public String addSpitterFromForm(@Valid Spitter spitter,
                                     BindingResult bindingResult,
                                     @RequestParam(value = "image", required = false) MultipartFile image) {

        if (bindingResult.hasErrors()) {
            return "spitters/edit";
        }
        spitterService.saveSpitter(spitter);
        return "redirect:/spitters/" + spitter.getUsername();
    }

    @RequestMapping(value = "/{username}", method = RequestMethod.GET)
    public String showSpitterProfile(@PathVariable String username,
                                     Model model) {
        model.addAttribute(spitterService.getSpitter(username));
        return "spitters/view";
    }

    @RequestMapping(value = "/{username}", method = RequestMethod.GET,
            params = "edit")
    public String editSpitterProfile(@PathVariable String username,
                                     Model model) {
        model.addAttribute(spitterService.getSpitter(username));
        return "spitters/edit";
    }

    @RequestMapping(value = "/{username}", method = RequestMethod.POST)
    public String updateSpitterFromForm(@PathVariable String username,
                                        @Valid Spitter spitter) {
        spitterService.saveSpitter(spitter);
        return "redirect:/spitters/" + username;
    }

    @RequestMapping(value = "/{username}", method = RequestMethod.DELETE)
    public String deleteSpitter(@PathVariable String username) {
        spitterService.deleteSpitter(username);
        return "redirect:/home";
    }

    @RequestMapping(value = "/{username}/spittles",
            method = RequestMethod.GET)
    public String listSpittlesForSpitter(
            @PathVariable String username, Model model) {
        model.addAttribute(spitterService.getSpitter(username));
        List<Spittle> spittlesForSpitter = spitterService.getSpittlesForSpitter(username);
        model.addAttribute(spittlesForSpitter);
        return "spittles/list";
    }
}
