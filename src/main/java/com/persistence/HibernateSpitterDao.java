package com.persistence;

import com.domain.Spitter;
import com.domain.Spittle;
import org.hibernate.SessionFactory;
import org.hibernate.classic.Session;
import org.hibernate.criterion.Restrictions;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Repository;

import java.util.List;

@Repository
public class HibernateSpitterDao implements SpitterDao {
    private SessionFactory sessionFactory;

    @Autowired
    public HibernateSpitterDao(SessionFactory sessionFactory) {
        this.sessionFactory = sessionFactory;
    }

    private Session curentSession() {
        return sessionFactory.getCurrentSession();
    }

    @Override
    public void addSpitter(Spitter spitter) {
        curentSession().save(spitter);
    }

    @Override
    public void saveSpitter(Spitter spitter) {
        curentSession().update(spitter);
    }

    @Override
    public Spitter getSpitterById(long id) {
        return (Spitter) curentSession().get(Spitter.class, id);
    }

    @Override
    public List<Spittle> getRecentSpittle() {
        return curentSession().createCriteria(Spittle.class).list();
    }

    @Override
    public void saveSpittle(Spittle spittle) {
        curentSession().save(spittle);
    }

    @Override
    public List<Spittle> getSpittlesForSpitter(Spitter spitter) {
        return curentSession().createCriteria(Spittle.class).add(Restrictions.eq("spitter.id", spitter.getId())).list();
    }

    @Override
    public Spitter getSpitterByUsername(String username) {
        return (Spitter) curentSession().createCriteria(Spitter.class).add(Restrictions.eq("username", username)).list().get(0);
    }

    @Override
    public void deleteSpittle(long id) {
        curentSession().delete(getSpittleById(id));
    }

    @Override
    public Spittle getSpittleById(long id) {
        return (Spittle) curentSession().get(Spittle.class, id);
    }

    @Override
    public List<Spitter> findAllSpitters() {
        return curentSession().createCriteria(Spitter.class).list();
    }

    @Override
    public void deleteSpitter(String username) {
        curentSession().delete(getSpitterByUsername(username));
    }
}
