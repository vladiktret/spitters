<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@ taglib prefix="sf" uri="http://www.springframework.org/tags/form" %>
<%@ taglib prefix="s" uri="http://www.springframework.org/tags" %>

<h2>Spitters</h2>
<c:forEach items="${spitters}" var="spitter">
  <s:url value="/spitters/${spitter.username}" var="spitter_url" />
  <a href="${spitter_url}"><c:out value="${spitter.username}"/></a> - <c:out value="${spitter.fullName}"/>
  <br>
</c:forEach>
<s:url value="/spitters" var="spitters_url"/>
<sf:form method="get" action="${spitters_url}" name="createSpittle">
  <input hidden="hidden" name="new"/>
  <input type="submit" value="Create"/>
</sf:form>