<%@ taglib prefix="sf" uri="http://www.springframework.org/tags/form" %>
<%@ taglib prefix="s" uri="http://www.springframework.org/tags" %>
<h2>Spitter</h2>
<h3>${spitter.username}</h3>
<p>${spitter.fullName}</p>
<s:url value="/spitters/${spitter.username}" var="spitter_url"/>
<sf:form method="get" action="${spitter_url}" name="createSpittle">
    <input hidden="hidden" name="edit"/>
    <input type="submit" value="Edit"/>
</sf:form>
<a href="${spitter_url}/spittles">Spittles</a>